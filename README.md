# AVRCD - AVR Controller with Display

Process control board on AVR MCU with display, ARDUINO compatible.

* ARDUINO compatible
* ATMega324 | ATMega644 | ATMega1284
* LCD display 2 x 16 chars
* RTC wit supercapacitor
* RS485
* Header for serial to USB converter
* Header ehternet module W5500 or W5100
* Bootloader see: https://gitlab.com/krupkaf/arduinobootloader
